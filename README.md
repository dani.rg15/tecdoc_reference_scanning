# Reference Scanning Script


Script to scan an input file of references and generates a file indicating whether the matching with TecDoc is successful. This script relies the REST API defined in the project https://gitlab.com/dani.rg15/tecdoc-articles-service.