<?php 
use GuzzleHttp\Client;
require './auth.conf.php';

function requestArticle($supplierCode = null, $articleNumber) {
	$client = new Client([
	    'base_uri' => 'http://165.227.148.148:3000',
	]);

	if ($supplierCode) {
		$query = [
			'token' => AUTH_TOKEN,
			'supplierCode' => $supplierCode,
			'articleNumber' => $articleNumber,
		];
	}
	else {
		$query = [
			'token' => AUTH_TOKEN,
			'articleNumber' => $articleNumber,
		];
	}

	$response = $client->request('GET', '/api/articles', [
		'query' => $query
	]);

	if ($response->getStatusCode() == 200) {
		return json_decode($response->getBody(), true);
	}

	return null;
}

?>