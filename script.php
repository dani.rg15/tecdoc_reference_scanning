<?php 
require 'vendor/autoload.php';
require 'lib.php';

if (!isset($argv[1])) {
    echo "Missing argument";
    exit(-1);
}

$handler_input = fopen($argv[1], "r");
$handler_output = fopen(str_replace('.csv', '', $argv[1]).'_output.csv', "w");

$index = 0;
while (($data = fgetcsv($handler_input, 1000, ';')) !== FALSE) {
    $index++;
    $time_start = microtime(true); 

    //Standard request 
    $requestedSupplierCode = $data[0];
    $requestedArticleNumber = $data[1];

    $articles = requestArticle($requestedSupplierCode, $requestedArticleNumber);

    if ($articles != null) {    	
        //Case 1: Perfect match
    	if ($articles['status'] == 'OK' && 
            $articles['articles'][0]['supplierCode'] == $requestedSupplierCode &&
    		$articles['articles'][0]['articleNumber'] == $requestedArticleNumber) {
    		$data['status'] = 'OK';
    	}
        //Case 2: Macth found, but miswritten reference
    	else if ($articles['status'] == 'OK' && 
            $articles['articles'][0]['supplierCode'] == $requestedSupplierCode &&
    		$articles['articles'][0]['articleNumber'] != $requestedArticleNumber) {
    		$data['status'] = 'WRONG';
    		$data['replace_with'] = $response['articles'][0]['articleNumber'];
    	}
        //Case 3: No match
    	else {
    		$data['status'] = 'UNKNOWN';
            $data['replace_with'] = '';

            //Request without supplier
            $articles = requestArticle(null, $data[1]);

            //Case 3.1: Exist one match using just the reference
            if($articles != null && count($articles['articles']) == 1) {
                $data['possible_supplier'] = $articles['articles'][0]['supplierName'];
                $data['supplier_code'] = $articles['articles'][0]['supplierCode'];
                $data['supplier_reference'] = $articles['articles'][0]['articleNumber'];
            }

    	}

    } else {
    	$data['status'] = 'Request failed';
    }
   
    fputcsv($handler_output,$data,';');
    
    $time_end = microtime(true);
    $execution_time = number_format(($time_end - $time_start)*1000, 2) ;
    echo "Index: $index Time: $execution_time ms \n";
}

fclose($handler_input);
fclose($handler_output);


?>